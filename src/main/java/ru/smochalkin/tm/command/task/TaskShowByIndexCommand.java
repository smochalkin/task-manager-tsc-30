package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        @NotNull final Task task = serviceLocator.getTaskService().findByIndex(userId, --index);
        showTask(task);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
