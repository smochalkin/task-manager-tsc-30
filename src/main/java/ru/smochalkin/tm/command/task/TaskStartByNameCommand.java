package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-start-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Start task by name.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findByName(userId, name);
        serviceLocator.getTaskService().updateStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
