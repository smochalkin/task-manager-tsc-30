package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter project name: ");
        @NotNull final String projectName = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isProjectName(userId, projectName)) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectTaskService().removeProjectByName(userId, projectName);
        System.out.println("[OK]");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
