package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-status-update-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project status by name.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(userId, name);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        @NotNull final Status status = Status.getStatus(statusName);
        serviceLocator.getProjectService().updateStatusByName(userId, name, status);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
