package ru.smochalkin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public interface SystemUtil {

    static long getPID() {
        @Nullable final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (isEmpty(processName)) return 0;
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (@NotNull final Exception e) {
            return 0;
        }
    }

}
