package ru.smochalkin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.data.fasterxml.BackupLoadCommand;
import ru.smochalkin.tm.command.data.fasterxml.BackupSaveCommand;


public class Backup extends Thread {

    private final int interval = 30000;

    @NotNull
    public final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(interval);
                save();
            } catch (Exception e) {
                bootstrap.getLogService().info("Backup info: " + e.getMessage());
            }
        }
    }

}


